.. cobu documentation master file, created by
   sphinx-quickstart on Sat Sep 15 13:16:27 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cobu's documentation!
================================


Do you maintain multiple versions of config files for different scenarios and keep
copying them to right location? Do you have this one set of information common to all
your config files and are tired of maintaining it over multiple files? Then
``cobu`` is your *config builder* of choice!

It allows you to split your config file in multiple fragments and combine them
as needed, but not only for one type of config files, i.e. not for only for one
tool, but for all of them. I, for example, like to have consistent colorschemes
across applications which i can change quickly, e.g. light schemes for the day
and dark schemes at night. This is achieved with the help of `jinja2`_ templates,
allowing you to reuse most of your existing config files and only changing the parts in
which they differ.



.. _jinja2: http://jinja.pocoo.org/
