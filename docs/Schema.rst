Schema files
============

Schemas contain the *building plans* of your configuration files. They are simple
`TOML`_ files typically named ``configfile_to_build.schema``.

The following schema is used to generate my configuration for `mako`_:

.. code-block:: toml

  default_variant = "default"
  comment_char= "#"
  hooks = "schema_hooks.py"

  [variants.default]
  description = "Default setup"
  fragments = [ "config.d/basics", "config.d/colors.j2" ]

It is a rather small example but should give an easy first impression how a schema file
is build.

Variants
--------

Variants are one key feature of ``cobu``. They enable to specify a different set of
files from which your final config is built. I use it to manage different variants of my
`_sway` config file (``notebook``, ``work``, ``home``) which i.e. differ in the way my
output devices are configured (position, resolution,...). These files are called
*fragments*.

.. _TOML: https://github.com/toml-lang/toml
.. _mako: https://github.com/emersion/mako
.. _sway: https://github.com/swaywm/sway
