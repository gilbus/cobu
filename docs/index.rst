.. include:: ../README.rst

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   Schema

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
